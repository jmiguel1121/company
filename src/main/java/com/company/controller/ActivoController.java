package com.company.controller;

import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.company.model.Activo;
import com.company.service.ActivoService;
import com.company.util.Util;

@RestController
@RequestMapping("/api/es-activos/v1")
public class ActivoController {

	@Autowired
	private ActivoService activoService;
	private String msg;

	/**
	 * get all activos from database
	 * 
	 * @throws Exception
	 */
	@GetMapping("/activo")
	public List<Activo> getAllActivos() throws Exception {
		return activoService.getAllActivo();
	}

	/**
	 * create activo from database
	 */
	@PostMapping("/activo")
	public HashMap createActivo(@RequestBody @Valid Activo activo) {
		try {
			activoService.createActivo(activo);
			msg = "activo creado exitosamente";
		} catch (Exception e) {
			// TODO: handle exception
			msg = e.getMessage();
		}
		return Util.responseMessage("message", msg);
	}

	/**
	 * update activo from database
	 */
	@PutMapping("/activo")
	public HashMap editActivo(@RequestBody @Valid Activo activo) {
		try {
			activoService.editActivo(activo);
			msg = "activo editado exitosamente";
		} catch (Exception e) {
			// TODO: handle exception
			msg = e.getMessage();
		}
		return Util.responseMessage("message", msg);
	}

	/**
	 * delete activo from database
	 */
	@DeleteMapping("/activo/{id}")
	public HashMap deleteActivo(@PathVariable("id") int activo) {
		try {
			activoService.deleteActivo(activo);
			msg = "activo eliminado exitosamente";
		} catch (Exception e) {
			// TODO: handle exception
			msg = e.getMessage();
		}
		return Util.responseMessage("message", msg);
	}
}
