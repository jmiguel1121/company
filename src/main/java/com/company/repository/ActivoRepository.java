package com.company.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.company.model.Activo;

@Repository
public interface ActivoRepository extends MongoRepository<Activo, Long> {

}
