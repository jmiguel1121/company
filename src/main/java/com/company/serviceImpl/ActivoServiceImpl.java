package com.company.serviceImpl;

import java.util.List;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.company.model.Activo;
import com.company.repository.ActivoRepository;
import com.company.service.ActivoService;

@Service
public class ActivoServiceImpl implements ActivoService {

	@Autowired
	private ActivoRepository activoRepository;

	/**
	 * 
	 */
	@Override
	public List<Activo> getAllActivo() throws Exception {
		try {
			return activoRepository.findAll();
		} catch (Exception e) {
			Logger.getLogger(ActivoServiceImpl.class.getName()).log(Logger.Level.ERROR, e);
			throw new Exception("Error inesperado");
		}
	}

	/**
	 * 
	 */
	@Override
	public boolean createActivo(Activo activo) throws Exception {
		try {
			return (activoRepository.save(activo) != null) ? true : false;
		} catch (Exception e) {
			Logger.getLogger(ActivoServiceImpl.class.getName()).log(Logger.Level.ERROR, e);
			throw new Exception("Error inesperado");
		}
	}

	/**
	 * 
	 */
	@Override
	public boolean editActivo(Activo activo) throws Exception {
		try {
			return (activoRepository.save(activo) != null) ? true : false;
		} catch (Exception e) {
			Logger.getLogger(ActivoServiceImpl.class.getName()).log(Logger.Level.ERROR, e);
			throw new Exception("Error inesperado");
		}
	}

	/**
	 * 
	 */
	@Override
	public boolean deleteActivo(int activo) throws Exception {
		Long id = new Long(activo);
		try {
			Activo act = new Activo();
			act = activoRepository.findById(id).get();
			activoRepository.delete(act);
			return true;
		} catch (Exception e) {
			Logger.getLogger(ActivoServiceImpl.class.getName()).log(Logger.Level.ERROR, e);
			throw new Exception("Error inesperado");
		}
	}

}
